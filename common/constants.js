/**
 * Created by sarthak on 19/2/16.
 */


function define(name, value){
  Object.defineProperty(exports, name, {
    value: value,
    enumerable: true
  });
}

define("NAME_MIN_LEN", 6);
define("NAME_MAX_LEN", 20);
define("ADDRESS_MIN_LEN", 10);
define("ADDRESS_MAX_LEN", 30);
define("PASSWORD_MIN_LEN", 8);
define("PASSWORD_MAX_LEN", 20);
define("ADDRESS_REGEX",/^[a-zA-z0-9\s,'-']*$/);
define("NAME_REGEX",/^[a-zA-Z ]*$/);
define("CUSTOMER_ACCESS","customer");
define("ADMIN_ACCESS","admin")
define("CUSTOMER_ACCESS_TOKEN_TIME", 129600);           // 3 months
define("ADMIN_ACCESS_TOKEN_TIME",120);                  // 6 hours

define("DESTROY_EXPIRED_ACCESS_TOKEN_TIME", 60);       // 1 hours
