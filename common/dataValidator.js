/**
 * Created by sarthak on 17/2/16.
 */

var validator = require('validator');
var constants = require('./constants');
var app = require("../server/server");
function isEmpty(obj){
  return !Object.keys(obj).length;
}

function isMobile(mobileNum){
  if(typeof mobileNum == 'string' && mobileNum.length >= 10 && mobileNum.length <= 13){
    return true;
  }
  return false;
}

function isName(name){
  var re = constants.NAME_REGEX;
  if ( name.length >= constants.NAME_MIN_LEN && name.length <= constants.NAME_MAX_LEN && re.test(name) && name != "string")
    return true;
  return false;
}
function isAddress(address){
  var re = constants.ADDRESS_REGEX;
  if (address.length >= constants.ADDRESS_MIN_LEN && address.length <= constants.ADDRESS_MAX_LEN && re.test(address) && address != "string")
    return true;
  return false;
};

function isPassword(password){
  return (password.length >= constants.PASSWORD_MIN_LEN && password.length <= constants.PASSWORD_MAX_LEN);
}

module.exports = {

  isValidMobile : function (mobile) {
    return !!isMobile(mobile);
  },

  isValidCustomerField : function(name, mobile, password, billingAddress) {
    if (billingAddress != null) {
      return (isName(name) && isAddress(billingAddress) && isPassword(password) && isMobile(mobile))
    }
    else
      return (isName(name) && isMobile(mobile) && isPassword(password));
  },

  validateCustomerCredentials : function (mobile) {
    return !!(isMobile(credentials.mobile) && isPassword(credentials.password));

  },

  validateLoginCredentials : function(mobile, password){
    return !!(typeof mobile == 'string' && isPassword(password));
  },

  validateAccessCredentials : function (adminId, adminMobile, adminAccessTokenId) {
    console.log("In validate access credentials--------");
    console.log("id is "+adminId);
    console.log("mobile is "+adminMobile);
    console.log("accessTokenId is "+adminAccessTokenId);
    if(!adminId || !adminMobile || !adminAccessTokenId){
      return false;
    }
    if(!isMobile(adminMobile)|| (typeof adminId != 'string') || (typeof adminAccessTokenId != 'string')){
      return false;
    }
    return true;
  }
};
