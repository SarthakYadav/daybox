var validator = require('validator');
var dataValidator = require("../../common/dataValidator");
var app = require("../../server/server");
var constants = require("../../common/constants");
var utils = require("../../common/utilities");
var accessType = constants.ADMIN_ACCESS;

module.exports = function(Admin) {
  Admin.disableRemoteMethod("create", true);
  Admin.disableRemoteMethod("upsert", true);
  Admin.disableRemoteMethod("updateAll", true);
  Admin.disableRemoteMethod("updateAttributes", true);
  //Admin.disableRemoteMethod("find", true);
  Admin.disableRemoteMethod("findById", true);
  Admin.disableRemoteMethod("findOne", true);
  //Admin.disableRemoteMethod("destroyById", true);
  //Admin.disableRemoteMethod("destroyAll", true);
  //Admin.disableRemoteMethod("deleteById", true);


  /**
   * Used by the Admin to log into the system
   * @param credentials : object type: credentials.mobile and credentials.password
   * @param cb  : callback function
   */
  Admin.login = function (credentials, cb) {
    var AccessTokenx = Admin.app.models.AccessTokenx;
    if(!dataValidator.validateLoginCredentials(credentials.mobile, credentials.password)){
      cb(utils.getGenericError("Invalid credential fields", 403, "Wrong credential fields"));
      return;
    }

    Admin.findOne({where: {mobile: credentials.mobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance){
        cb(utils.getGenericError("Admin Mobile field didnt match", 403, "Access Denied"));
        return;
      }
      utils.comparePassword(credentials.password, instance.password, function (err, isMatched) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(!isMatched){
          cb(utils.getGenericError("Password Didnt Match", 403, "Access denied"));
          return;
        }

        utils.manageAccessToken(AccessTokenx ,null, instance.id, accessType, function (err, accessTokenInstanceId) {
          if(err){
            cb(utils.getInternalServerError(err));
            return;
          }

          var response = {};
          response.id = instance.id;
          response.name = instance.name;
          response.mobile = instance.mobile;
          response.accessTokenId = accessTokenInstanceId;
          cb(null, response);
        });
      });
    });
  };

  Admin.remoteMethod(
    'login',
    {
      description: "For logging in",
      accepts: {arg: 'credentials', type: 'object', required: true},
      returns: {arg: 'response', type: 'object'},
      http: {path: '/login' , verb: 'post' }
    }
  );

  /**
   *    For logging-out an Admin
   * @param {required} contains adminId, accessTokenId, adminMobile
   * @param cb
     */

  Admin.logout = function (required, cb) {
    if(!dataValidator.validateAccessCredentials(required.adminId, required.adminMobile, required.accessTokenId)){
      cb(utils.getGenericError("Invalid Required Field", 403, "Error"));
      return;
    }

    Admin.findOne({include: {relation : 'accessTokenxs', scope:{where: {id: required.accessTokenId}}}, where: {id: required.adminId}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }
      if(!instance){
        cb(utils.getGenericError("Whoa. Something went wrong", 403, "Error"));
        return;
      }

      instance.accessTokenxs.destroy(required.accessTokenId, function(err){
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }
        var response = true;
        cb(null, response);
        return;
      });
    });
  };

  Admin.remoteMethod(
    'logout',
    {
      description: "For logging out Admin",
      accepts: {arg: 'required', type: 'object', required: true},
      returns: {arg: 'response', type: 'object'},
      http: {path: '/logout' , verb: 'post' }
    }
  );

  /**
   *  For adding a new Customer object
   *
   * @param query contains Customer data, ie. customerName, customerMobile, customerPassword, customerBillAdd,orderIdList
   * @param required contains adminId, adminMobile,accessTokenId
   * @param cb
     */
  Admin.addCustomer = function (query, required, cb) {

    var AccessTokenx = Admin.app.models.AccessTokenx;
    var currentTime = new Date();

    if(!dataValidator.validateAccessCredentials(required.adminId, required.adminMobile, required.accessTokenId)){
      cb(utils.getGenericError("Invalid Required Field", 403, "Error"));
      return;
    }

    Admin.findOne({include: {relation: 'accessTokenxs', scope:{where: {id: required.accessTokenId}}}, where: {id: required.adminId, mobile: required.adminMobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Authentication Failure", 403, "Access Denied"));
        return;
      }

      console.log("Printing orderIdList");
      console.log(JSON.stringify(query.orderIdList));
      var itemsList = new Array();


      // as of now necessary. Gotta find a better way
      setTimeout(function() {
        // Whatever you want to do after the wait
        console.log(itemsList);

        var obj = {};
        obj.name = query.customerName;
        obj.mobile = query.customerMobile;
        obj.password = query.customerPassword;
        if(query.customerBillAdd)
          obj.billingAddress = query.customerBillAdd;
        obj.itemsList = itemsList;
        app.models.Customer.findOrCreate({where: {mobile: query.customerMobile, name: query.customerName}}, obj, function (err, instance) {
          if(err){
            cb(utils.getInternalServerError(err));
            return;
          }
          if(instance) {
            cb(null, instance);
          }
          else
            cb(utils.getGenericError("Something went terribly wrong", 403, "Error"));
        });
      }, 3000);
      for(var i = 0;i< query.orderIdList.length; i++){
        app.models.OrderItemsList.findOne({where: {id:query.orderIdList[i]}}, function (err, orderItemInstance) {
          if(err){
            cb(utils.getInternalServerError(err));
            return;
          }
          if(!orderItemInstance){
            cb(utils.getGenericError("Couldn't Find said Item", 403, "Error"));
            return;
          }
          var item = {name:orderItemInstance.name, id:orderItemInstance.id};
          itemsList.push(item);
        });
      }

    });
  };


  Admin.remoteMethod(
    'addCustomer',
    {
      description: "For adding Customers",
      accepts : [
        {arg : 'query', type:'object', required: true},
        {arg : 'required', type: 'object', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/addCustomer', verb : 'post'}
    }
  );

  /**
   * Used to Remove an existing customer
   * @param customerMobile contains mobile num of the customer to delete
   * @param credentials contains adminId,adminMobile, accessTokenId
   * @param cb
   */
  Admin.removeCustomer = function (customerMobile, credentials, cb) {
    var currentTime = new Date();

    if(!dataValidator.validateAccessCredentials(credentials.adminId, credentials.adminMobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Required Field", 403, "Error"));
      return;
    }

    Admin.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.adminId, mobile: credentials.adminMobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Authentication Failed", 403, "Access Denied"));
        return;
      }

      app.models.Customer.findOne({mobile: customerMobile}, function(err, instance){
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(!instance){
          cb(utils.getGenericError("No such User exists", 403, "Error"));
          return;
        }

        instance.destroy(function(err){
          if(err){
            cb(utils.getInternalServerError(err));
            return;
          }
          var response = {}
          response.isDeleted = true;
          cb(null,response);
          return;
        });
      });
    });
  };

  Admin.remoteMethod(
    'removeCustomer',
    {
      description: 'For removing existing Customer',
      accepts: [
        {arg : 'customerMobile', type: 'object', required: true, http: { source: 'body' }},
        {arg : 'credentials', type: 'object', required: true, http: { source: 'body' }}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path : '/removeCustomer', verb: 'delete'}
    }
  );

  /**
   *    For updating an existing customer, or if it doesnt exist, create a new One
   * @param credentials contains adminId,adminMobile, accessTokenId
   * @param query contains customerName, customerMobile, customerPassword, customerBillingAddress,orderIdList {all pertaining to the Customer}
   * @param cb
   *
   */
  Admin.updateCustomer = function (credentials, query, cb) {
    var currentTime = new Date();

    if(!dataValidator.validateAccessCredentials(credentials.adminId, credentials.adminMobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Credentials Field", 403, "Error"));
      return;
    }

    var itemsList = new Array();

    for(var i = 0;i< query.orderIdList.length; i++){
      app.models.OrderItemsList.findById(query.orderIdList[i], function (err, orderItemInstance) {

        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(!orderItemInstance){
          cb(utils.getGenericError("Couldn't Find said Item", 403, "Error"));
          return;
        }

        var item = {name:orderItemInstance.name, id:orderItemInstance.id};
        itemsList.push(item);
      });
    }

    var obj = {};
    obj.name = query.customerName;
    obj.mobile = query.customerMobile;
    obj.password = query.customerPassword;
    if(query.customerBillingAddress)
      obj.billingAddress = query.customerBillingAddress;

    obj.itemsList = itemsList;

    Admin.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.adminId, mobile: credentials.adminMobile}}, function (err, instance){
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Authentication Failed", 403, "Access Denied"));
        return;
      }

      app.models.Customer.upsert(obj, function (err, instance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(instance) {
          var response = {};
          response.isUpdated = true;
          cb(null, response);
          return;
        }
      });
    });
  };

  Admin.remoteMethod(
    'updateCustomer',
    {
      description: "For updating existing customer",
      accepts: [
        {arg: 'credentials', type: 'object', required: true , http: { source: 'body' }},
        {arg: 'query', type: 'object', required: true, http: { source: 'body' }}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/updateCustomer', verb: 'post'}
    }
  );

  /**
   *
   * @param credentials contains adminId, adminMobile, accessTokenId
   * @param query contains name, mobile, password
   * @param cb
     */
  Admin.addNewAdmin  = function (credentials, query, cb) {
    var currentTime = new Date();
    console.log("printing credentials : "+credentials);
    console.log("printing query : "+query);
    if(!dataValidator.validateAccessCredentials(credentials.adminId, credentials.adminMobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Credentials", 403, "Access denied"));
      return;
    }

    Admin.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.adminId, mobile: credentials.adminMobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }

      Admin.findOrCreate({where: {mobile: query.mobile}}, query, function (err, createdInstance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }
        else if(!createdInstance){
          cb(utils.getGenericError("Something went terribly wrong", 403, "Error"))
          return;
        }
        else{
          var response = {};
          response.createdAdmin = createdInstance;
          response.isSuccessfull = true;
          cb(null, response);
          return;
        }
      });
    });
  };

  Admin.remoteMethod(
    'addNewAdmin',
    {
      description: 'For Adding another Admin',
      accepts: [
        {arg : 'credentials', type: 'object', required: true},
        {arg: 'query', type: 'object', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/addNewAdmin', verb: 'post'}
    }
  );

  /**
   * Method for removing Arguments
   * @param credentials contains adminId, adminMobile, accessTokenId
   * @param query contains mobile, password
   * @param cb
     */
  Admin.removeAdmin = function(credentials, query, cb){
    var currentTime = new Date();
    if(!dataValidator.validateAccessCredentials(credentials.adminId, credentials.adminMobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Credentials", 403, "Access Denied"));
      return;
    }

    Admin.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.adminId, mobile: credentials.adminMobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }

      Admin.findOne({where: {mobile: query.mobile, password: query.password}}, function (err, instance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(!instance){
          cb(utils.getGenericError("No such Admin found", 403, "Error"));
          return;
        }

        instance.destroy(function (err) {
          if(err){
            cb(utils.getInternalServerError(err));
            return;
          }

          var response = {};
          response.deletionSuccessful = true;
          cb(null, response);
          return;
        });
      });
    });
  };

  Admin.remoteMethod(
    'removeAdmin',
    {
      description: 'For deleting an admin',
      accepts: [
        {arg: 'credentials', type: 'object', required: true},
        {arg: 'query', type: 'object', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/removeAdmin', verb: 'post'}
    }
  );

  /**
   *  For updating the global list of items available
   *
   * @param credentials contains adminId, adminMobile, accessTokenId
   * @param orderItems consists of item name and id, in alphabetical order
   *                    must continue from where the original list left off
   *                    eg.
   *
   *                    [
   *                      {"name":"Potatoes","id":68},
   *                      {"name":"SweetPotatoes","id":69},
   *                      ....
   *                    ]
   * @param cb
   */
  Admin.updateGlobalItemsList = function(credentials, orderItems, cb){
    var currentTime = new Date();
    if(!dataValidator.validateAccessCredentials(credentials.adminId, credentials.adminMobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Credentials", 403, "Access Denied"));
      return;
    }

    Admin.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.adminId, mobile: credentials.adminMobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }
      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }

      app.models.OrderItemsList.upsert(orderItems, function (err, orderItemsInstance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }
        if(!instance){
          cb(utils.getGenericError("Couldnt Update global Order List", 403, "Error"));
          return;
        }
        var response = {};
        response.isSuccess = true;
        cb(null, response);
      });
    });
  };

  Admin.remoteMethod(
    'updateGlobalItemsList',
    {
      description: 'For deleting an admin',
      accepts: [
        {arg: 'credentials', type: 'object', required: true},
        {arg: 'orderItems', type: 'object', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/updateGlobalItemsList', verb: 'post'}
    }
  );

  /**
   *  Gets the latest order made by a certain Customer
   * @param credentials contains adminId, adminMobile, accessTokenId
   * @param data contains customerMobile and customerName
   * @param cb
     */
  Admin.getLatestOrder = function (credentials, data, cb) {
    var currentTime = new Date();

    if(!dataValidator.validateAccessCredentials(credentials.adminId, credentials.adminMobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Required Field", 403, "Error"));
      return;
    }

    Admin.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.adminId, mobile: credentials.adminMobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Authentication Failed", 403, "Access Denied"));
        return;
      }

      app.models.Customer.findOne({include: {relation: 'orders', where: {name:data.customerName, mobile:data.customerMobile}}}, function (err, customerInstance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }
        cb(null, customerInstance.orders()[0]);
        return;
      });
    });
  };

  Admin.remoteMethod(
    'getLatestOrder',
    {
      description: 'Returns the latest order placed by a particular customer',
      accepts: [
        {arg: 'credentials', type: 'object', required: true},
        {arg: 'data', type: 'object', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/getLatestOrder', verb: 'post'}
    }
  );
};
