var validator = require('validator');
var dataValidator = require("../../common/dataValidator");
var app = require("../../server/server");
var constants = require("../../common/constants");
var utils = require("../../common/utilities");
var accessType = constants.CUSTOMER_ACCESS;
//var loopback = require('loopback');
module.exports = function(Customer) {

  //Customer.disableRemoteMethod("create", true);
  Customer.disableRemoteMethod("upsert", true);
  Customer.disableRemoteMethod("updateAll", true);
  Customer.disableRemoteMethod("updateAttributes", true);
  Customer.disableRemoteMethod("find", true);
  Customer.disableRemoteMethod("findById", true);
  //Customer.disableRemoteMethod("findOne", true);
  Customer.disableRemoteMethod("destroyById", true);
  Customer.disableRemoteMethod("destroyAll", true);
 // Customer.disableRemoteMethod("deleteById", true);
  /**
   *  For logging-in a user
   * @param credentials contains the mobile and password
   * @param cb
   *
     */

  Customer.login = function (credentials, cb) {
    var AccessTokenx = Customer.app.models.AccessTokenx;
    if(!dataValidator.validateLoginCredentials(credentials.mobile, credentials.password)){
      cb(utils.getGenericError("Invalid credential fields", 403, "Wrong credential fields"));
      return;
    }

    Customer.findOne({where :{mobile: credentials.mobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance){
        cb(utils.getGenericError("No match found for mobile field", 403, "Invalid input"));
        return;
      }

      utils.comparePassword(credentials.password, instance.password, function (err, isMatched) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(!isMatched){
          cb(utils.getGenericError("Password Didnt Match", 403, "Access denied"));
          return;
        }

        utils.manageAccessToken(AccessTokenx, null, instance.id, accessType, function (err, accessTokenInstanceId) {
          if(err){
            cb(utils.getInternalServerError(err));
            return;
          }

          var response = {};
          response.id = instance.id;
          response.name = instance.name;
          response.mobile = instance.mobile;
          if(instance.billingAddress)
            response.billingAddress = instance.billingAddress;
          response.accessTokenId = accessTokenInstanceId;
          cb(null, response);
          return;
        });
      });
    });
  };

  Customer.remoteMethod(
    'login',
    {
      description: "For logging in",
      accepts: {arg: 'credentials', type: 'object', required: true, http: { source: 'body' }},
      returns: {arg: 'response', type: 'object'},
      http: {path: '/login' , verb: 'post' }
    }
  );

  /**
   *  For logging-out a Customer
   * @param credentials contains id, mobile, and accessTokenId
   * @param cb
   *
   */
  Customer.logout = function (credentials, cb) {
    var currentTime = new Date();
    if(!dataValidator.validateAccessCredentials(credentials.id, credentials.mobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Required Field", 403, "Error"));
      return;
    }

    Customer.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.id, mobile: credentials.mobile}}, function (err, instance){
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance || currentTime > instance.accessTokenxs()[0].expiry){
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }
      instance.accessTokenxs.destroy(credentials.accessTokenId, function (err) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        var response = true;
        cb(null, response);
        return;
      });
    });
  };

  Customer.remoteMethod(
    'logout',
    {
      description: "For logging out Customer",
      accepts: {arg: 'credentials', type: 'object', required: true},
      returns: {arg: 'response', type: 'object'},
      http: {path: '/logout' , verb: 'post' }
    }
  );

  /**
   *    TO place an order
   *
   * @param credentials contains id, mobile, password
   * @param orderList is a list of orders , declaring orders by their id
   *        in the following format
   *            [{"id":id_of_item,"qty":quantity},......]
   * @param cb
     */
  Customer.placeOrder = function(credentials, orderList, cb){
    var Order = Customer.app.models.Order;
    function createOrder(orderList, instance) {
      var itemsList = instance.itemsList;
      var order = new Array();

      for(var i = 0; i < orderList.length; i++) {
        for(var j = 0; j < itemsList.length; j++) {
          if(itemsList[j].id == orderList[i].id){
            var item = {name:itemsList[j].name, id:orderList[i].id, qty:orderList[i].qty};
            order.push(item);
          }
        }
      }
      return order;
    }
    var currentTime = new Date();
    if(!dataValidator.validateAccessCredentials(credentials.id, credentials.mobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Credentials.", 403, "Access Denied"));
      return;
    }

    Customer.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.id, mobile: credentials.mobile}}, function (err, instance) {
      if (err) {
        cb(utils.getInternalServerError(err));
        return;
      }
      if (!instance || currentTime > instance.accessTokenxs()[0].expiry) {
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }
      var itemList = createOrder(orderList, instance);
      console.log("PRinting order");
      console.log(JSON.stringify(itemList));
      utils.createOrder(Order, instance.id, itemList, function (err, orderInstance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(!instance){
          cb(utils.getGenericError("Couldnt Create Order", 403, "Error"));
          return;
        }
        cb(null, orderInstance);
        return;

      });
    });
  };

  Customer.remoteMethod(
    'placeOrder',
    {
      description: 'For Placing an admin',
      accepts: [
        {arg: 'credentials', type: 'object', required: true},
        {arg: 'orderList', type: 'object', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/placeOrder', verb: 'post'}
    }
  );

  /**
   *  Returns order by it's id
   * @param credentials contains id, mobile, password
   * @param data contains orderId
   * @param cb
     */
  Customer.getOrder = function (credentials, data, cb) {
    var currentTime = new Date();
    if(!dataValidator.validateAccessCredentials(credentials.id, credentials.mobile, credentials.accessTokenId)){
      cb(utils.getGenericError("Invalid Credentials.", 403, "Access Denied"));
      return;
    }

    Customer.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.id, mobile: credentials.mobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if (!instance || currentTime > instance.accessTokenxs()[0].expiry) {
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }

      app.models.Order.findById(data.orderId, function (err, orderInstance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }

        if(!orderInstance) {
          cb(utils.getGenericError("Cannot find Said ORder", 403, "Error"));
          return;
        }
        cb(null, orderInstance);
      });
    });
  };

  Customer.remoteMethod(
    'getOrder',
    {
      description: 'For Getting order by ID',
      accepts: [
        {arg: 'credentials', type: 'object', required: true},
        {arg: 'data', type: 'object', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/getOrder', verb: 'post'}
    }
  );

  /**
   * Returns all orders made by a Customer
   * param credentials contains id, mobile, accessTokenId
   * @param cb
     */
  Customer.getAllOrders = function(credentials, cb){
    var currentTime = new Date();
    console.log(JSON.stringify(credentials));
    //if(!dataValidator.validateAccessCredentials(credentials.id, credentials.mobile, credentials.accessTokenId)){
    //  cb(utils.getGenericError("Access Credentials not valid", 403, "Access Denied"));
    //  return;
    //}

    Customer.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.id, mobile: credentials.mobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }
      if(!instance){
        cb(utils.getGenericError("No such accessToken found", 403, "Error"));
        return;
      }
      if (currentTime > instance.accessTokenxs()[0].expiry) {
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }

      app.models.Order.find({where: {userId: credentials.id}}, function (err, instances) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }
        if(!instances){
          cb(utils.getGenericError("No orders by Given user", 403, "Error"));
          return;
        }
        cb(null, instances);
      });
    });
  };

  Customer.remoteMethod(
    'getAllOrders',
    {
      description: 'For Getting all orders of a Customer',
      accepts: {arg: 'credentials', type: 'object', required: true, http: { source: 'body' }},
      returns: {arg: 'response', type: 'object'},
      http: {path: '/getAllOrders', verb: 'get'}
    }
  );

  Customer.getItemsList = function (credentials, cb) {
    var currentTime = new Date();
    //if(!dataValidator.validateAccessCredentials(credentials.id, credentials.mobile, credentials.accessTokenId)){
    //  cb(utils.getGenericError("Access Credentials not valid", 403, "Access Denied"));
    //  return;
    //}
    console.log(credentials);
    Customer.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.id, mobile: credentials.mobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }
      if(!instance){
        cb(utils.getGenericError("No such accessToken found", 403, "Error"));
        return;
      }
      if (currentTime > instance.accessTokenxs()[0].expiry) {
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }
      cb(null, instance.itemsList);
      return;
    });
  };

  Customer.remoteMethod(
    'getItemsList',
    {
      description: 'For Getting all orders of a Customer',
      accepts: {arg: 'credentials', type: 'object', required: true, http: { source: 'body' }},
      returns: {arg: 'response', type: 'object'},
      http: {path: '/getItemsList', verb: 'get'}
    }
  );

  Customer.getOrderByDate = function (credentials, date, cb) {
    var currentTime = new Date();
    Customer.findOne({include: {relation: 'accessTokenxs', scope: {where: {id: credentials.accessTokenId}}}, where: {id: credentials.id, mobile: credentials.mobile}}, function (err, instance) {
      if(err){
        cb(utils.getInternalServerError(err));
        return;
      }

      if(!instance){
        cb(utils.getGenericError("No such accessToken found", 403, "Error"));
        return;
      }
      if (currentTime > instance.accessTokenxs()[0].expiry) {
        cb(utils.getGenericError("Expired Access Token", 403, "Access Denied"));
        return;
      }

      app.models.Order.findOne({where: {userId: credentials.id, timeStamp: date}}, function (err, instance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }
        if(!instance){
          cb(utils.getGenericError("No orders by Given user", 403, "Error"));
          return;
        }
        cb(null, instance);
      });
    });
  };

  Customer.remoteMethod(
    'getOrderByDate',
    {
      description: 'For Getting Order by timeStamp',
      accepts: [
        {arg: 'credentials', type: 'object', required: true, http: { source: 'body' }},
        {arg: 'date', type:'date', required: true}
      ],
      returns: {arg: 'response', type: 'object'},
      http: {path: '/getOrderByDate', verb: 'get'}
    }
  );
};
