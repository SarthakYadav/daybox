/**
 * Created by sarthak on 19/2/16.
 */


var constants = require('./constants');
var uuid = require('uuid4');
var app = require("../server/server");
module.exports = {

  comparePassword : function (password, userPassword, cb) {
    if(password == userPassword)
      cb(null,true);

    else if(password != userPassword)
      cb(null,false);

    else{
      var error = new Error("Unknown error");
      error.statusCode = 500;
      error.name = "Error";
      cb(error);
    }
  },
  getGenericError : function (errorMessage, errorStatusCode, errorName) {
    var error = new Error(errorMessage);
    error.statusCode = errorStatusCode;
    error.name = errorName;
    return error;
  },

  getInternalServerError : function (err) {
    console.log(err);
    var error = new Error('Something went wrong. Try again!');
    error.statusCode = 500;
    error.name = "Internal Server Error";
    return error;
  },

  manageAccessToken : function (AccessTokenx, accessTokenId, userId, accessType, cb) {
    var currentTime = new Date();
    if(accessTokenId){
      process.nextTick(function(){
        AccessTokenx.findById(accessTokenId, function (err,accessTokenInstance) {
          if(err){
            console.log(err);
            var error = new Error("Invalid AccessToken id");
            error.statusCode = 500;
            error.name = "AccessToken Error";
            cb(error);
            return;
          }
          if(accessTokenInstance){
            if(accessType == constants.CUSTOMER_ACCESS)
              accessTokenInstance.expiry = new Date(currentTime.getTime() + constants.CUSTOMER_ACCESS_TOKEN_TIME*60000);
            if(accessType == constants.ADMIN_ACCESS)
              accessTokenInstance.expiry = new Date(currentTime.getTime() + constants.ADMIN_ACCESS_TOKEN_TIME*60000);
            accessTokenInstance.save(function (err,instance) {
              if(err){
                console.log(err);
                var error = new Error("Error Saving AccessToken.");
                error.statusCode = 500;
                error.name = "AccessToken Error";
                cb(error);
                return;
              }
              cb(null,instance.id);
              return;
            });
          }
          else{
            var accessToken = {};
            accessToken.id= uuid();
            accessToken.created = currentTime;
            accessToken.userId = userId;

            if(accessType == constants.CUSTOMER_ACCESS)
              accessToken.expiry = new Date(currentTime.getTime() + constants.CUSTOMER_ACCESS_TOKEN_TIME*60000);
            if(accessType == constants.ADMIN_ACCESS)
              accessToken.expiry = new Date(currentTime.getTime() + constants.ADMIN_ACCESS_TOKEN_TIME*60000);

            AccessTokenx.create(accessToken, function (err, accessTokenInstance) {
              if(err){
                console.log(err);
                var error = new Error("Error Creating AccessToken");
                error.statusCode = 500;
                error.name = "AccessToken Error";
                cb(error);
                return;
              }
              cb(null, accessTokenInstance.id);
              return;
            });
          }
        });
      });
    }
    else{
      process.nextTick(function() {
        var accessToken = {};
        accessToken.id = uuid();
        accessToken.created = currentTime;
        accessToken.userId = userId;

        if(accessType == constants.CUSTOMER_ACCESS)
          accessToken.expiry = new Date(currentTime.getTime() + constants.CUSTOMER_ACCESS_TOKEN_TIME*60000);
        if(accessType == constants.ADMIN_ACCESS)
          accessToken.expiry = new Date(currentTime.getTime() + constants.ADMIN_ACCESS_TOKEN_TIME*60000);

        AccessTokenx.create(accessToken, function(err, accessTokenInstance){
          if(err){
            console.log(err);
            var error = new Error("Error creating AccessToken");
            error.statusCode = 500;
            error.name = "AccessToken error";
            cb(error);
            return;
          }
          cb(null, accessTokenInstance.id);
          return;
        });
      });
    }
  },

  createOrder : function (Order, userId, itemsList, cb) {
    var currentTime = new Date();

    var obj = {};
    obj.timeStamp = currentTime;
    obj.items = itemsList;
    obj.userId = userId;
    Order.create(obj, function (err, instance) {
      if(err){
        cb(err);
        return;
      }
      if(!instance){
        console.log(err);
        var error = new Error("Error Creating order");
        error.statusCode = 500;
        error.name = "ORder error";
        cb(error);
      }
      cb(null, instance);
      return;
    });
  },

  getOrder : function(query){
    var namesList = new Array();
    for(var i = 0; i < query.qtyList.length; i++){
      app.models.OrderItemsList.findById(query.idList[i], function (err, orderItemInstance) {
        if(err){
          cb(utils.getInternalServerError(err));
          return;
        }
        if(!orderItemInstance){
          cb(utils.getGenericError("Couldn't Find said Item", 403, "Error"));
          return;
        }
        namesList.push(orderItemInstance.name);
      });
    }
    console.log(JSON.stringify(namesList));
    var items = new Array();
    for(var i = 0; i < namesList.length; i++){
      items.push([{id:query.idList[i]}, {name:namesList[i]}, {qty:query.qtyList[i]}]);
    }
    console.log("In getOrder");
    console.log(items);
    return items;
  }

};
