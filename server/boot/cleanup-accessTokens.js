/**
 * Created by sarthak on 20/2/16.
 */
var scheduler=require('node-schedule');
var constants=require("../../common/constants");
var cronString="*/"+constants.DESTROY_EXPIRED_ACCESS_TOKEN_TIME+" * * * *";

module.exports = function(app) {

  /*
   *   this function call to the scheduler's scheduleJob method
   *   schedules a repetitive job that deletes expired tokens
   *   automatically executes every "DESTROY_ACCESS_TOKEN_TIME" minutes
   */
  scheduler.scheduleJob(cronString,function(){
    var cTime=new Date();

    app.models.AccessTokenx.destroyAll({expiry : {lt: cTime}},function(error,items){
      if(error)
        console.log(error);
      if(items.count!=0)
        console.log("No of expired accessTokens destroyed : "+items.count);
      console.log("Scheduled accessToken destroyer");
      console.log(cTime);
    });
  });
};
