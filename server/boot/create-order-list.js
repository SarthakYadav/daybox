/**
 * Created by sarthak on 20/2/16.
 */
var orderItems = require('../../common/order-items.json');

module.exports = function(app) {
  app.dataSources.mongoDs.automigrate('OrderItemsList', function(err) {
    if (err) throw err;

    app.models.OrderItemsList.upsert(orderItems, function(err, orderItemsInstance) {
      if (err) throw err;

      console.log('-----orderItems created--------- \n');
      console.log(orderItemsInstance);
    });
  });
};

